# The Wren scripting system

Over the course of starting PAYDAY 2, playing a game, and quitting to the menu at least four Lua instances
will be started and stopped. These instances all have their own variables, and in some cases run concurrently.

Developments to SuperBLT lead to the need for an environment where code would start, and run, in one consistent
context for the duration of the game. For technical reasons we can't easily use another Lua instance, so instead
another scripting language called Wren has been embedded in SuperBLT.

The documentation for this language can be found at [wren.io](https://wren.io).

This can be used to make very deep changes to the game, particularly involving asset handling. This environment
is where the XML tweaker is implemented, for example. However regular mods can also use Wren scripts to do their
own advanced asset-related stuff.

In a `supermod.xml` file, you can use the following to load a Wren file at startup:

```xml
<?xml version="1.0"?>
<mod>
	<wren>
		<init file="my_wren_file"/>
	</wren>
</mod>
```

This will load the wren file found at `mods/your-mod/wren/my_wren_file.wren` - note that all your Wren script
files *must* be placed inside a directory named `wren`.

The 'Hello, World' sample here is as follows:

```wren
import "base/native" for Logger
Logger.log("Hello, World!")
```

This will then show up in your console and log file.

To do more stuff, please see the other pages in the Wren section of this documentation.
