# SuperBLT

SuperBLT is a fork of the BLT mod-loading hook for PAYDAY 2, with a number of major
improvements, such as a [cross-platform audio API](doc/xaudio.md) and the ability to
[alter any base-game XML file](doc/tweaker.md) without the hassle of modifying bundle files
(greatly alleviating the need for Bundle Modder).

## Installation

### Prerequisite

If you've previously used vanilla BLT, you will have to:

- Delete `IPHLPAPI.dll` - This is the original BLT DLL, and you can't have both installed at
the same time due to conflicts.
- Delete `mods/base` - This is another part of the original BLT, and SuperBLT will download its
own version. However, you must first delete the old version.

### Windows 

- Download and install the [Microsoft Visual C++ 2017 Redistributable package (x86)](https://aka.ms/vs/15/release/VC_redist.x86.exe)
(you will only need to do this once, even if you later reinstall SuperBLT)
- Download the [Latest Release DLL](https://sblt-update.znix.xyz/pd2update/download/get.php?src=homepage&id=payday2bltwsockdll),
and place it in your `PAYDAY 2` folder (alongside `payday2_win32_release.exe`).
- Now, start the game, and SuperBLT will prompt you to download the basemod. Select `Yes`, and it will
notify you when it's done downloading. At this point, SuperBLT has been fully installed.

### Linux

- Make sure the game is running through Proton, to do so, right-click on the game then go to Properties. In the `Compatibility` tab make sure the box is checked and select `Proton Experimental`
- Add these Launch options in the `General` tab in the Properties of the game: `WINEDLLOVERRIDES="wsock32=n,b" %command%`
- Download the [Latest Release DLL](https://sblt-update.znix.xyz/pd2update/download/get.php?src=homepage&id=payday2bltwsockdll),
and place it in your `PAYDAY 2` folder (alongside `payday2_win32_release.exe`).
- Now, start the game, and SuperBLT will prompt you to download the basemod. Select `Yes`, and it will
notify you when it's done downloading. At this point, SuperBLT has been fully installed.

## Regarding the `IPHLPAPI.dll` vs. `WSOCK32.dll` situation

Some computers, for reasons that are not apparent, refuse to load the `IPHLPAPI.dll` file as
used by vanilla BLT. This seems to primarily affect Windows 10 systems. For this reason,
SuperBLT has switched over to hooking `WSOCK32.dll` instead, which I've yet to see any trouble
from.

If, for whatever reason, you require an `IPHLPAPI.dll` version, you can download
the [IPHLPAPI Release DLL](https://sblt-update.znix.xyz/pd2update/download/get.php?src=homepage&id=payday2bltdll), and
install it like you would with the normal (i.e. `WSOCK32.dll`) version. Just don't install both
at the same time.

## Development version

If you want to try the most recent version of the DLL for some reason you can get it from [AppVeyor](https://ci.appveyor.com/project/ZNix/payday2-superblt/build/artifacts).
These versions contain the most recent changes that haven't been published officially yet and may have new features or minor bugfixes.
Just note that the ingame download manager will claim there is a new version for the DLL if you use anything other than the release version of it.

## Credits

- ZNixian, for writing SuperBLT
- the Restoration Crew, for testing/feedback
- JamesWilko and SirWaddles, for writing the original BLT

## Source Code
This project is Free and Open Source software, with the DLL under the GNU General
Public License version 3 (or any later version), and the basemod under the BSD license.

[DLL Source Code](https://gitlab.com/znixian/payday2-superblt)

[Basemod Source Code](https://gitlab.com/znixian/payday2-superblt-lua)

## Developer Documentation

Almost all of the SuperBLT provided Lua functions are annotated and will show hints when working in an editor like [VS Code](https://code.visualstudio.com/).
For a general overview of BLT functionality, see [the vanilla BLT's documentation](https://payday-2-blt-docs.readthedocs.io/en/latest/).

Documentation for features added by SuperBLT can be found in the site navigation menu.
